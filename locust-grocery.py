from random import choice
from locust import HttpUser, task, between, SequentialTaskSet

USER_CREDENTIALS = [
    ("eleazertoluan", "LykaTesting"),
    ("reshu1611", "M@llTest!"),
]
USER_CACHE = []
ORDER_ID_CACHE = dict()  ### cache in which the order_id corresponds to the base_header


class UserBehaviour(SequentialTaskSet):

    def on_start(self):
        self.user_name = ''
        self.password = ''
        self.user_id = ''
        self.first_address_id = ''
        self.phone_number = ''
        self.base_header = dict()
        self.order_id = ''
        self.productID = ''

        if len(USER_CREDENTIALS) > 0:
            self.user_name, self.password = USER_CREDENTIALS.pop()

            form_data = {
                "countryCode": "PH",
                "username": self.user_name,
                "password": self.password,
                "device": {
                    "deviceId": "CD355C33-2F68-4975-B292-B623C08F660F",
                    "deviceOs": "iOS",
                    "deviceModel": "iPhone 11 Pro Max",
                    "osVersion": "14.0",
                    "deviceName": "iOS"
                }
            }

            self.base_header = {
                'Connection': 'keep-alive',
                'deviceOs': 'ios',
                'Content-Type': 'application/json',
                'Request-Origin-App': 'Lyka-App',
                'User-Agent': 'LykaShopDemo/1 CFNetwork/1121.2.2 Darwin/19.2.0'
            }
            with self.client.post("https://identity-awsdev.mylykaapps.com/useraccounts/login", json=form_data,
                                  headers=self.base_header,
                                  catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed, EXCEPTION: " + response.text)
                self.base_header['Authorization'] = "Bearer " + response.json()['data']['token']['accessToken']
                self.user_id = response.json()['data']['user']['id']

                with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/profiles/GetUserProfileForEditing",
                                     headers=self.base_header,
                                     catch_response=True) as response1:
                    if response1.status_code != 200:
                        response1.failure("Failed to fetch the user profile, EXCEPTION: " + response1.text)

                with self.client.get("http://buyers-dev.api.lyka.app/api/v1/address/" + str(self.user_id), name = "http://buyers-dev.api.lyka.app/api/v1/address/",
                                     headers=self.base_header,
                                     catch_response=True) as response:
                    if response.status_code != 200:
                        response.failure("Failed to fetch the user profile, EXCEPTION: " + response.text)
                    self.first_address_id = response.json()['data'][0]['id']
                    self.phone_number = response.json()['data'][0]['contactNumber']

            USER_CACHE.append({
                'user_name': self.user_name,
                'password': self.password,
                'base_header': self.base_header,
                'user_id': self.user_id,
                'user_first_address_id': self.first_address_id,
                'phone_number': self.phone_number,
            })

    @task
    def place_order(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if not USER_CREDENTIALS and USER_CACHE:
            data = choice(USER_CACHE)
            self.user_name = data['user_name']
            self.password = data['password']
            self.base_header = data['base_header']
            self.user_id = data['user_id']
            self.first_address_id = data['user_first_address_id']
            self.phone_number = data['phone_number']

        if self.user_name:
            form_data = {
                "userId": str(self.user_id),
                "username": str(self.user_name),
                "userPhoneNumber": str(self.phone_number),
                "additionalInfo": "If an item is not available, remove it from my order.",
                "city" : "Angat",
                "device":{
                  "deviceId": "72b4d86ab843c98a",
                  "deviceImei": "",
                  "deviceModel": "samsung SM-G9980",
                  "deviceName": "android",
                  "deviceOs": "Android R ",
                  "isEmulator": False,
                  "notificationToken": "cMXpYhQQT2e_V9pOMnHet6:APA91bFaJGC23C2oowWIGW48jXhkLM1bPyp6jErj2ofag8UGBx_XwWqW2utKm52ICdktglbU7G4KyKPu7C-4RXirE9DblHiyb5CsvhqgNnCXRfvIga6flNgCYpDzlP4LvqK3RBQMCjTH",
                  "osVersion":"30"
               },
               "products":[
                    {
                    "quantity" : 1,
                                    "id": "062de4e1-cfda-4cd7-a507-2dcc5a8b10da",
                                    "productSkuId": "67a02b42-b2dc-4590-adfa-e2bf92d8b631",
                                    "sellerId": "ae0285a4-f3f2-475d-8753-d9bf375f4ada"
                    }
               ],
                            "promoId": "78794883-10fe-4600-ba85-57de02507d11",
                            "shippingInfo":"fc482dad-7d60-4059-8502-6e875df52491"
                        }

            with self.client.post("http://grocery-orders-dev.api.lyka.app/api/v1/order", headers=self.base_header,
                                  json=form_data,
                                  catch_response=True) as response:
                if response.status_code != 201:
                    response.failure("Failed to fetch the user profile, EXCEPTION: " + response.text)

                self.order_id = response.json()['results']['orderReferenceNumber']

            ORDER_ID_CACHE[
                self.order_id] = self.base_header  ## add the order_id and its corresponding base_header to the cache

    @task
    def cancel_order(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if self.order_id in ORDER_ID_CACHE:  # If the order_id is in the cache, take the base_header from there
            self.base_header = ORDER_ID_CACHE[self.order_id]
            global form_data
            form_data = {

                "orderReferenceNumber": str(self.order_id),
                "orderedProducts": [
                    {
                        "skuId": "67a02b42-b2dc-4590-adfa-e2bf92d8b631",
                        "quantity": 1,
                        "remarks": {
                            "reason": "Not Specified - Test Buyer"
                        }
                    }
                ]
            }

            with self.client.post("http://grocery-orders-dev.api.lyka.app/api/v1/order/user-cancel-items",
                                  json=form_data, headers=self.base_header, catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed to fetch the cancel the order, EXCEPTION: " + response.text)
                else:
                    ORDER_ID_CACHE.pop(
                        self.order_id)  # delete order_id from cache, if the order is successfully cancelled


class User(HttpUser):
    tasks = [UserBehaviour]
    wait_time = between(1, 2)