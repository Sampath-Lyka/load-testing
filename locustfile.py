from random import choice
from locust import HttpUser, task, between, SequentialTaskSet

USER_CREDENTIALS = [
    ("eleazertoluan", "LykaTesting"),
    ("reshu1611", "M@llTest!"),
]
USER_CACHE = []
ORDER_ID_CACHE = dict()  ### cache in which the order_id corresponds to the base_header


class UserBehaviour(SequentialTaskSet):

    def on_start(self):
        self.user_name = ''
        self.password = ''
        self.user_id = ''
        self.first_address_id = ''
        self.phone_number = ''
        self.base_header = dict()
        self.order_id = ''
        self.productID = ''

        if len(USER_CREDENTIALS) > 0:
            self.user_name, self.password = USER_CREDENTIALS.pop()

            form_data = {
                "countryCode": "PH",
                "username": self.user_name,
                "password": self.password,
                "device": {
                    "deviceId": "CD355C33-2F68-4975-B292-B623C08F660F",
                    "deviceOs": "iOS",
                    "deviceModel": "iPhone 11 Pro Max",
                    "osVersion": "14.0",
                    "deviceName": "iOS"
                }
            }

            self.base_header = {
                'Connection': 'keep-alive',
                'deviceOs': 'ios',
                'Content-Type': 'application/json',
                'Request-Origin-App': 'Lyka-App',
                'User-Agent': 'LykaShopDemo/1 CFNetwork/1121.2.2 Darwin/19.2.0'
            }
            with self.client.post("https://identity-awsdev.mylykaapps.com/useraccounts/login", json=form_data,
                                  headers=self.base_header,
                                  catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed, EXCEPTION: " + response.text)
                self.base_header['Authorization'] = "Bearer " + response.json()['data']['token']['accessToken']
                self.user_id = response.json()['data']['user']['id']

                with self.client.get("https://gateway-awsdev.mylykaapps.com/api/v3/profiles/GetUserProfileForEditing",
                                     headers=self.base_header,
                                     catch_response=True) as response1:
                    if response1.status_code != 200:
                        response1.failure("Failed to fetch the user profile, EXCEPTION: " + response1.text)

                with self.client.get("http://buyers-dev.api.lyka.app/api/v1/address/" + str(self.user_id),
                                     name="http://buyers-dev.api.lyka.app/api/v1/address/",
                                     headers=self.base_header,
                                     catch_response=True) as response:
                    if response.status_code != 200:
                        response.failure("Failed to fetch the user profile, EXCEPTION: " + response.text)
                    self.first_address_id = response.json()['data'][0]['id']
                    self.phone_number = response.json()['data'][0]['contactNumber']

            USER_CACHE.append({
                'user_name': self.user_name,
                'password': self.password,
                'base_header': self.base_header,
                'user_id': self.user_id,
                'user_first_address_id': self.first_address_id,
                'phone_number': self.phone_number,
            })

    @task
    def place_order(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if not USER_CREDENTIALS and USER_CACHE:
            data = choice(USER_CACHE)
            self.user_name = data['user_name']
            self.password = data['password']
            self.base_header = data['base_header']
            self.user_id = data['user_id']
            self.first_address_id = data['user_first_address_id']
            self.phone_number = data['phone_number']

        if self.user_name:
            form_data = {
                "userId": str(self.user_id),
                "username": str(self.user_name),
                "userPhoneNumber": str(self.phone_number),
                "device": {
                    "deviceId": "7F620B68-2E16-4707-9275-39B6FB57FC61",
                    "deviceOs": "AND",
                    "deviceModel": "OnePlus7",
                    "osVersion": "11",
                    "isEmulator": False,
                    "deviceName": "AND"
                },
                "products": [
                    {
                        "quantity": 1,
                        "productSkuId": "2172e78e-30d6-46e3-8c6e-3594a8cdc445",
                        "productId": "37d6a71f-623f-4d21-a39e-e2477c0362c5",
                        "sellerId": "9dc78e57-da50-4e58-b861-6344ec4e0637"
                    }
                ],
                "promoId": "da40d834-a5e7-4d09-a41b-0398119c864a",
                "shippingInfo": str(self.first_address_id),
                "additionalInfo": "",
                "gemTransactionNumber": ""
            }

            with self.client.post("http://orders-dev.api.lyka.app/api/v1/order", headers=self.base_header,
                                  json=form_data,
                                  catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed to create an order with ," + str(form_data) + " The response after failure "
                                                                                           "is : " + response.text)

                self.order_id = response.json()['results']['orderReferenceNumber']

            ORDER_ID_CACHE[
                self.order_id] = self.base_header  ## add the order_id and its corresponding base_header to the cache

    @task
    def get_order_details(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if self.order_id in ORDER_ID_CACHE:  # If the order_id is in the cache, take the base_header from there
            self.base_header = ORDER_ID_CACHE[self.order_id]
            with self.client.get("http://orders-dev.api.lyka.app/api/v1/order/" + self.order_id,
                                 name="http://orders-dev.api.lyka.app/api/v1/order/",
                                 headers=self.base_header, catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed to get order details for" + str(self.order_id) + " The response after "
                                                                                              "failure: " +
                                     response.text)

                self.productID = response.json()['results']['orderedProducts'][0]['orderedProductIds'][0]
                print(self.productID)

    @task
    def cancel_order(self):
        # user should be logged in here (unless the USER_CREDENTIALS ran out)
        if self.order_id in ORDER_ID_CACHE:  # If the order_id is in the cache, take the base_header from there
            self.base_header = ORDER_ID_CACHE[self.order_id]
            global form_data
            form_data = [
                {
                    "remarks": {
                        "reason": "Not specified"
                    },
                    "orderId": str(self.order_id),
                    "orderedProductIds": [
                        self.productID
                    ]
                }
            ]

            with self.client.post("http://orders-dev.api.lyka.app/api/v1/order/cancel-items",
                                  json=form_data, headers=self.base_header, catch_response=True) as response:
                if response.status_code != 200:
                    response.failure("Failed to cancel the order with the payload," + str(form_data) + " The response "
                                                                                                       "after the "
                                                                                                       "failure is: "
                                                                                                       "" +
                                     response.text)
                else:
                    ORDER_ID_CACHE.pop(
                        self.order_id)  # delete order_id from cache, if the order is successfully cancelled


class User(HttpUser):
    tasks = [UserBehaviour]
    wait_time = between(1, 2)
